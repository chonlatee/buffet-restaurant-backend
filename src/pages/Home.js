import React from 'react';
// import PropTypes from 'prop-types';

import { connectRedux } from '../utils';

const Home = ({ users }) => {
  // const [valueInput, setValueInput] = useState('')
  // useEffect(() => {
  //   try {
  //     const { ipcRenderer } = window.require('electron');
  //     ipcRenderer.on('user-reply', (event, arg) => {
  //       if (arg !== null) setUsers(JSON.parse(arg))
  //     })
  //   } catch (err) {
  //     console.error(err);
  //   }
  // }, [])
  
  // const handleCreateUser = () => {
  //   try {
  //     const { ipcRenderer } = window.require('electron');
  //     ipcRenderer.send('create-user', { username: valueInput });
  //     setValueInput('')
  //   } catch (err) {
  //     console.log('err', err);
  //   }
  // }

  // const getUser = () => {
  //   try {
  //     const { ipcRenderer } = window.require('electron');
  //     ipcRenderer.send('get-user');
  //   } catch (err) {
  //     console.log('err', err);
  //   }
  // }

  return (
    <div className="container">
      <h1>Home Page</h1>
        <h2>User</h2>
        <ul>
          {users.map((value) => (<li key={value.username}>{value.username}</li>))}
        </ul>
    </div>
  );
};

Home.propTypes = {
  // dispatch: PropTypes.func.isRequired
};

const mapState = ({ user }) => ({ users: user.users });

export default connectRedux(mapState)(Home);
