import React from 'react';
import { MemoryRouter as Router } from 'react-router-dom';
import { Global, css } from '@emotion/core';
import PropTypes from 'prop-types';
import io from 'socket.io-client';

import Routers from './Routers';
import { connectRedux } from './utils';
import { getAllUser } from './actions/action';

import { Navigation } from './components';

const globalStyles = css``;

const mapState = ({ user }) => ({ isLogin: user.isLogin });

const App = (props) => {
  React.useEffect(() => {
    console.log('props >>', props);

  const socket = io('http://localhost:3333');
  socket.on('users', (users) => {
    props.dispatch(getAllUser(users));
  });
  }, []); // eslint-disable-line

  return (
  <Router>
    <div className="wrap-page">
      <Global styles={globalStyles} />
      <Navigation />
      <Routers isLogin={props.isLogin} />
    </div>
  </Router>
)};

App.propTypes = {
  isLogin: PropTypes.bool.isRequired
};

export default connectRedux(mapState)(App);
