import * as ActionType from './actionType';

const getAllUser = (users) => ({
  type: ActionType.GET_ALL_USER,
  users
});

export { getAllUser }