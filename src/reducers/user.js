import * as ActionTypes from '../actions/actionType';

const innitialState = {
  isLogin: true,
  users: []
};

export default (state = innitialState, action) => {
  switch (action.type) {
    case ActionTypes.GET_ALL_USER: {
      return { ...state, users: action.users }
    }
    default: {
      return state;
    }
  }
};
